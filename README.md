# TagSense  Plugin

TagSense is a service that allows for external processing of aircraft tags in the `VATSIM` network. This `Euroscope plugin` interfaces with the [TagSense API](https://gitlab.com/portugal-vacc/tagsense-api) at the location specified by each vACC.

---
### Alpha Phase
The TagSense plugin is currently in the alpha phase, meaning there is a chance that Euroscope ( being Euroscope ) will completely wet it's pants. It is currently being tested at Portugal vACC. If you would like to take part in the tests, please join the [Portugal vACC Discord Server](https://discord.portugal-vacc.org) for instructions. Here are the main features being planned:
- [ ] `Config file/menu` <sub>( custom server address, aircraft details to send, update frequency, ... )</sub>

- [ ] `Euroscope commands`
